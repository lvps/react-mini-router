import React from "react";
import { Route, Router, useHistory } from "../../src/index";
import Foo from "./bar.jsx";
import Bar from "./foo.jsx";

const RouteMap = () => {
  return (
    <Router>
      <Links />
      <Route path="foo" component={<Foo />} />
      <Route path="bar" component={<Bar />} />
    </Router>
  );
};

const Links = () => {
  const history = useHistory();

  const go = (path) => {
    history.push(path, { name: path });
  };

  return (
    <div className="btns">
      <button onClick={() => go("foo")}>foo</button>
      <button onClick={() => go("bar")}>bar</button>
    </div>
  );
};

const App = () => {
  return (
    <div>
      <RouteMap />
    </div>
  );
};

export default App;
