import { useContext } from "react";
import { RouterContext } from "./router";

export function useLocation() {
  const { location } = useContext(RouterContext);
  return location;
}

export function useHistory() {
  const { history } = useContext(RouterContext);
  return history;
}
