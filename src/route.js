import { useLocation } from "./hooks";

// Route组件，主要功能就是判断当前的location中的path和自身的path是否匹配，如果匹配，就返回自身设置的组件
export const Route = ({ path, children, component }) => {
  const { pathname } = useLocation()

  const match = path === pathname

  if(match) {
    return component || children
  }

  return null
};
