const path = require("path");
const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./example/index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
  },
  devtool: 'source-map',
  devServer: {
    port: 3000,
  },
  module: {
    rules: [
      {
        test: /\.js[x]?$/,
        use: ["babel-loader"],
      },
    ],
  },
  plugins: [
    new HTMLWebpackPlugin({
      template: "./example/index.html",
    }),
  ],
};
